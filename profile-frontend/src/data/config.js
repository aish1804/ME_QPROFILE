
module.exports = {
  // TODO: change your profile information here
  name: "Aishit Dua",
  greeting: "Hello 👋",
  greetingDescription: "I'm Aishit Dua.Currently working as a SDE 1 with Apple Maps for more than a year.Proficient in Python, Scala and Spark.",
  githubUrl: "https://github.com/aishitdua",
  linkedinUrl: "https://linkedin.com/in/aishit-dua-839aab160/",
  cvLink: "https://drive.google.com/file/d/1GZbW9Zo6GYM290cJIUdVIEzUYDqyXvYJ/view?usp=sharing",
};
